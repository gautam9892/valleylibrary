package com.mindvalley.android.valleylibrary.MockClasses;

import android.content.Context;
import android.net.http.AndroidHttpClient;
import android.os.Build;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpStack;

import java.io.File;

/**
 * Created by Gautam on 9/18/2016.
 */
public class MockVolley {

    /**
     * Default on-disk cache directory.
     */
    private static final String DEFAULT_CACHE_DIR = "volley";

    /**
     * Creates a default instance of the worker pool and calls {@link RequestQueue#start()} on it.
     *
     * @param context A {@link Context} to use for creating the cache dir.
     * @param stack   An {@link HttpStack} to use for the network, or null for default.
     * @return A started {@link RequestQueue} instance.
     */
    public static MockRequestQueue newMockRequestQueue(Context context, MockHttpStack stack) {
        File cacheDir = new File(context.getCacheDir(), DEFAULT_CACHE_DIR);

        String userAgent = "volley/0";
        try {
            //String packageName = context.getPackageName();
            //PackageInfo info = context.getPackageManager().getPackageInfo(packageName, 0);
            userAgent = "gautam" + "/" + 5;
        } catch (Exception e) {
        }

        if (stack == null) {
            if (Build.VERSION.SDK_INT >= 9) {
                stack = new MockHurlStack();
                //stack = new MockHttpClientStack(AndroidHttpClient.newInstance(userAgent));
            } else {
                // Prior to Gingerbread, HttpUrlConnection was unreliable.
                // See: http://android-developers.blogspot.com/2011/09/androids-http-clients.html
                stack = new MockHttpClientStack(AndroidHttpClient.newInstance(userAgent));
            }
        }

        MockNetwork network = new MockBasicNetwork(stack);

        MockRequestQueue queue = new MockRequestQueue(new MockDiskBasedCache(cacheDir), network);
        queue.start();

        return queue;
    }

    /**
     * Creates a default instance of the worker pool and calls {@link RequestQueue#start()} on it.
     *
     * @param context A {@link Context} to use for creating the cache dir.
     * @return A started {@link RequestQueue} instance.
     */
    public static MockRequestQueue newRequestQueue(Context context) {
        return newMockRequestQueue(context, null);
    }
}