package com.mindvalley.android.valleylibrary.MockClasses;

/**
 * Created by Gautam on 9/18/2016.
 */
public interface MockResponseDelivery {
    /**
     * Parses a response from the network or cache and delivers it.
     */
    public void postResponse(MockRequest<?> request, MockResponse<?> response);

    /**
     * Parses a response from the network or cache and delivers it. The provided
     * Runnable will be executed after delivery.
     */
    public void postResponse(MockRequest<?> request, MockResponse<?> response, Runnable runnable);

    /**
     * Posts an error for the given request.
     */
    public void postError(MockRequest<?> request, MockVolleyError error);
}

