package com.mindvalley.android.valleylibrary.MockClasses;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;

/**
 * Created by Gautam on 9/18/2016.
 */
public interface MockNetwork
{

    /**
 * Performs the specified request.
 * @param request Request to process
 * @return A {@link NetworkResponse} with data and caching metadata; will never be null
 * @throws VolleyError on errors
 */
public MockNetworkResponse performRequest(MockRequest<?> request) throws MockVolleyError, VolleyError;
}