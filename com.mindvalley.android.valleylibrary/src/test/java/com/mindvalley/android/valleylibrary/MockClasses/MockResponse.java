package com.mindvalley.android.valleylibrary.MockClasses;

/**
 * Created by Gautam on 9/18/2016.
 */
public class MockResponse<T> {

    /** Callback interface for delivering parsed responses. */
    public interface Listener<T> {
        /** Called when a response is received. */
        public void onResponse(T response);
    }

    /** Callback interface for delivering error responses. */
    public interface ErrorListener {
        /**
         * Callback method that an error has been occurred with the
         * provided error code and optional user-readable message.
         */
        public void onErrorResponse(MockVolleyError error);
    }

    /** Returns a successful response containing the parsed result. */
    public static <T> MockResponse<T> success(T result, MockCache.Entry cacheEntry) {
        return new MockResponse<T>(result, cacheEntry);
    }

    /**
     * Returns a failed response containing the given error code and an optional
     * localized message displayed to the user.
     */
    public static <T> MockResponse<T> error(MockVolleyError error) {
        return new MockResponse<T>(error);
    }

    /** Parsed response, or null in the case of error. */
    public final T result;

    /** Cache metadata for this response, or null in the case of error. */
    public final MockCache.Entry cacheEntry;

    /** Detailed error information if <code>errorCode != OK</code>. */
    public final MockVolleyError error;

    /** True if this response was a soft-expired one and a second one MAY be coming. */
    public boolean intermediate = false;

    /**
     * Returns whether this response is considered successful.
     */
    public boolean isSuccess() {
        return error == null;
    }


    private MockResponse(T result, MockCache.Entry cacheEntry) {
        this.result = result;
        this.cacheEntry = cacheEntry;
        this.error = null;
    }

    private MockResponse(MockVolleyError error) {
        this.result = null;
        this.cacheEntry = null;
        this.error = error;
    }
}
