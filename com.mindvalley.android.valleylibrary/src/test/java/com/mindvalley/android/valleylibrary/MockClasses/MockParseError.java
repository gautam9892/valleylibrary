package com.mindvalley.android.valleylibrary.MockClasses;

/**
 * Created by Gautam on 9/18/2016.
 */
public class MockParseError extends MockVolleyError {
    public MockParseError() { }

    public MockParseError(MockNetworkResponse networkResponse) {


        super(networkResponse);
    }

    public MockParseError(Throwable cause) {
        super(cause);
    }
}
