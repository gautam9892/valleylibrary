package com.mindvalley.android.valleylibrary.MockClasses;

/**
 * Created by Gautam on 9/18/2016.
 */
public class MockVolleyError extends Exception {
    public final MockNetworkResponse networkResponse;
    private long networkTimeMs;

    public MockVolleyError() {
        networkResponse = null;
    }

    public MockVolleyError(MockNetworkResponse response) {
        networkResponse = response;
    }

    public MockVolleyError(String exceptionMessage) {
        super(exceptionMessage);
        networkResponse = null;
    }

    public MockVolleyError(String exceptionMessage, Throwable reason) {
        super(exceptionMessage, reason);
        networkResponse = null;
    }

    public MockVolleyError(Throwable cause) {
        super(cause);
        networkResponse = null;
    }

    /* package */ void setNetworkTimeMs(long networkTimeMs) {
        this.networkTimeMs = networkTimeMs;
    }

    public long getNetworkTimeMs() {
        return networkTimeMs;
    }
}


