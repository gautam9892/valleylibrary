package com.mindvalley.android.valleylibrary;

import android.content.Context;
import android.util.Log;

import com.android.volley.ResponseDelivery;
import com.mindvalley.android.valleylibrary.MockClasses.MockJSONArray;
import com.mindvalley.android.valleylibrary.MockClasses.MockJsonArrayRequest;
import com.mindvalley.android.valleylibrary.MockClasses.MockNetwork;
import com.mindvalley.android.valleylibrary.MockClasses.MockResponse;
import com.mindvalley.android.valleylibrary.MockClasses.MockVolley;
import com.mindvalley.android.valleylibrary.MockClasses.MockVolleyError;
import com.mindvalley.android.valleylibrary.MockClasses.MockVolleyLog;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.mock;

/**
 * Unit Test to check basic functioning of the ValleyLibrary Document Parsing Functionalities
 * Created by Gautam on 9/21/2016.
 */

@Config(constants = BuildConfig.class,sdk = 18)
@RunWith(RobolectricGradleTestRunner.class)
public class ValleyLibraryUnitTest {





    private ResponseDelivery mDelivery;

    //MockNetwork Object
    @Mock
    private MockNetwork mMockNetwork;

    private String Json="";

    //Object to Wait on for the Main Thread until i receives a response
    static public Object monitor;

    //String obects to test the Return Results
    public String result="";
    public String demoSuccessString="";
    public String demoFailureString="";

    //Mock the Context Class
    Context ctx= mock(Context.class);

    //Tag to  to identify the Request
    public static final String TAG = AppController.class
            .getSimpleName();


    //Called before the test method
    @Before
    public void setUp() throws Exception {
        monitor  = new Object();

    }

    /**Makes a random cache en
     * Test to Pass in different URL to Check the Response returned with preSet string Objects
     */
    @Test
    public void jsonParseSuccess() throws Exception {


        final String urlTest = "http://pastebin.com/raw/wgkJgazE";
        final String urlTest1 = "http://cdn.crunchify.com/wp-content/uploads/code/jsonArray.txt";
        final String urlFailure = "http://www.androidbegin.com/tutorial/jsonparsetutorial.txt";

        MockJsonArrayRequest mockJsonArrayRequest = generateMockJsonArrayRequest(urlFailure);
        MockVolley.newRequestQueue(ctx).add(mockJsonArrayRequest);


        //Make the main thread wait for the Response
        //The Background thread will will up the main thread once it has finished executing the Response
        synchronized(monitor) {
            monitor.wait();
        }

    }


    //Test to test for a Successful String Match of Response and PreSet String Objects
    @After
    public void resultSuccess() throws Exception {
        setSuccessDemoString();
        assertThat(result,equalTo(demoSuccessString));
    }


    //Test to test for a Failed String Match of Response and PreSet String Objects
    @After
    public void resultFailure()
    {
        setFailureDemoString();
        assertThat(result,equalTo(demoFailureString));


    }

    //PreSet the String object demoFailureString
    public void setFailureDemoString()
    {
        demoFailureString="{ \n" +
                "\"worldpopulation\": \n" +
                "\t[\n" +
                "\t\t {\n" +
                "\t\t \"rank\":1,\"country\":\"China\",\n" +
                "\t\t \"population\":\"1,354,040,000\",\n" +
                "\t\t \"flag\":\"http://www.androidbegin.com/tutorial/flag/china.png\"\n" +
                "\t\t }, \n" +
                "\t\t\n" +
                "\t\t {\n" +
                "\t\t \"rank\":2,\"country\":\"India\",\n" +
                "\t\t \"population\":\"1,210,193,422\",\n" +
                "\t\t \"flag\":\"http://www.androidbegin.com/tutorial/flag/india.png\"\n" +
                "\t\t }, \n" +
                "\t\t \n" +
                "\t\t {\n" +
                "\t\t \"rank\":3,\"country\":\"United States\",\n" +
                "\t\t \"population\":\"315,761,000\",\n" +
                "\t\t \"flag\":\"http://www.androidbegin.com/tutorial/flag/unitedstates.png\"\n" +
                "\t\t }, \n" +
                "\t\t \n" +
                "\t\t {\n" +
                "\t\t \"rank\":4,\"country\":\"Indonesia\",\n" +
                "\t\t \"population\":\"237,641,326\",\n" +
                "\t\t \"flag\":\"http://www.androidbegin.com/tutorial/flag/indonesia.png\"\n" +
                "\t\t }, \n" +
                "\t\t \n" +
                "\t\t {\n" +
                "\t\t \"rank\":5,\"country\":\"Brazil\",\n" +
                "\t\t \"population\":\"193,946,886\",\n" +
                "\t\t \"flag\":\"http://www.androidbegin.com/tutorial/flag/brazil.png\"\n" +
                "\t\t }, \n" +
                "\t\t \n" +
                "\t\t {\n" +
                "\t\t \"rank\":6,\"country\":\"Pakistan\",\n" +
                "\t\t \"population\":\"182,912,000\",\n" +
                "\t\t \"flag\":\"http://www.androidbegin.com/tutorial/flag/pakistan.png\"\n" +
                "\t\t }, \n" +
                "\t\t \n" +
                "\t\t {\n" +
                "\t\t \"rank\":7,\"country\":\"Nigeria\",\n" +
                "\t\t \"population\":\"170,901,000\",\n" +
                "\t\t \"flag\":\"http://www.androidbegin.com/tutorial/flag/nigeria.png\"\n" +
                "\t\t }, \n" +
                "\t\t \n" +
                "\t\t {\n" +
                "\t\t \"rank\":8,\"country\":\"Bangladesh\",\n" +
                "\t\t \"population\":\"152,518,015\",\n" +
                "\t\t \"flag\":\"http://www.androidbegin.com/tutorial/flag/bangladesh.png\"\n" +
                "\t\t }, \n" +
                "\t\t \n" +
                "\t\t {\n" +
                "\t\t \"rank\":9,\"country\":\"Russia\",\n" +
                "\t\t \"population\":\"143,369,806\",\n" +
                "\t\t \"flag\":\"http://www.androidbegin.com/tutorial/flag/russia.png\"\n" +
                "\t\t }, \n" +
                "\t\t \n" +
                "\t\t {\n" +
                "\t\t \"rank\":10,\"country\":\"Japan\",\n" +
                "\t\t \"population\":\"127,360,000\",\n" +
                "\t\t \"flag\":\"http://www.androidbegin.com/tutorial/flag/japan.png\"\n" +
                "\t\t } \n" +
                "\t] \n" +
                "}";

    }

    //PreSet the String object demoFailureString
    public void setSuccessDemoString()
    {
        demoSuccessString="[\n" +
                "\t{\n" +
                "\t\tcolor: \"red\",\n" +
                "\t\tvalue: \"#f00\"\n" +
                "\t},\n" +
                "\t{\n" +
                "\t\tcolor: \"green\",\n" +
                "\t\tvalue: \"#0f0\"\n" +
                "\t},\n" +
                "\t{\n" +
                "\t\tcolor: \"blue\",\n" +
                "\t\tvalue: \"#00f\"\n" +
                "\t},\n" +
                "\t{\n" +
                "\t\tcolor: \"cyan\",\n" +
                "\t\tvalue: \"#0ff\"\n" +
                "\t},\n" +
                "\t{\n" +
                "\t\tcolor: \"magenta\",\n" +
                "\t\tvalue: \"#f0f\"\n" +
                "\t},\n" +
                "\t{\n" +
                "\t\tcolor: \"yellow\",\n" +
                "\t\tvalue: \"#ff0\"\n" +
                "\t},\n" +
                "\t{\n" +
                "\t\tcolor: \"black\",\n" +
                "\t\tvalue: \"#000\"\n" +
                "\t}\n" +
                "]";
    }

    /**
     * Create a Mock JsonArray Request.
     * @param url of the Json Document
     */
    private MockJsonArrayRequest generateMockJsonArrayRequest(String url)
    {


        MockJsonArrayRequest req = new MockJsonArrayRequest(url,
                new MockResponse.Listener<MockJSONArray>() {


                    @Override
                    public void onResponse(MockJSONArray response) {
                        Log.d(TAG,response.toString());
                        setString(response.toString());
                        try {

                            synchronized (monitor) {                 // 2
                                monitor.notifyAll();                  // 3
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new MockResponse.ErrorListener() {
            @Override
            public void onErrorResponse(MockVolleyError error) {
                MockVolleyLog.d(TAG, "Error: " + error.getMessage());
                setString(error.getMessage());
                synchronized(monitor) {                 // 2
                    monitor.notifyAll();                  // 3
                }

            }
        });

        return req;
    }


    public void setString(String str)
    {
        result=str;
    }



    /*
     HttpURLConnection urlConnection;

        StringBuilder result = new StringBuilder();

        try {
            URL url = new URL("https://api.github.com/users/dmnugent80/repos");
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }

        }catch( Exception e) {
            e.printStackTrace();
        }


     */



}
