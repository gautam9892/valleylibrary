package com.mindvalley.android.valleylibrary.MockClasses;

import android.os.Handler;

/**
 * Created by Gautam on 9/18/2016.
 */
public class MockExecutorDelivery implements MockResponseDelivery {
    /** Used for posting responses, typically to the main thread. */
    private final MockExecutor mResponsePoster;

    /**
     * Creates a new response delivery interface.
     * @param handler {@link Handler} to post responses on
     */
    public MockExecutorDelivery(final Handler handler) {
        // Make an Executor that just wraps the handler.
        mResponsePoster = new MockExecutor() {
            @Override
            public void execute(Runnable command) {
                handler.post(command);
            }
        };
    }

    /**
     * Creates a new response delivery interface, mockable version
     * for testing.
     * @param executor For running delivery tasks
     */
    public MockExecutorDelivery(MockExecutor executor) {
        mResponsePoster = executor;
    }

    @Override
    public void postResponse(MockRequest<?> request, MockResponse<?> response) {
        postResponse(request, response, null);
        //Log.i("fdffd","ddfdsf");
    }

    @Override
    public void postResponse(MockRequest<?> request, MockResponse<?> response, Runnable runnable) {
        request.markDelivered();
        request.addMarker("post-response");
        Thread thread = new Thread(new ResponseDeliveryRunnable(request, response, runnable));
        thread.start();
       // mResponsePoster.execute(new ResponseDeliveryRunnable(request, response, runnable));
    }

    @Override
    public void postError(MockRequest<?> request, MockVolleyError error) {
        request.addMarker("post-error");
        MockResponse<?> response = MockResponse.error(error);
        Thread thread = new Thread(new ResponseDeliveryRunnable(request, response, null));
        thread.start();
        //mResponsePoster.execute(new ResponseDeliveryRunnable(request, response, null));
    }

    /**
     * A Runnable used for delivering network responses to a listener on the
     * main thread.
     */
    @SuppressWarnings("rawtypes")
    private class ResponseDeliveryRunnable implements Runnable {
        private final MockRequest mRequest;
        private final MockResponse mResponse;
        private final Runnable mRunnable;

        public ResponseDeliveryRunnable(MockRequest request, MockResponse response, Runnable runnable) {
            mRequest = request;
            mResponse = response;
            mRunnable = runnable;
        }

        @SuppressWarnings("unchecked")
        @Override
        public void run() {
            // If this request has canceled, finish it and don't deliver.
            if (mRequest.isCanceled()) {
                mRequest.finish("canceled-at-delivery");

                return;
            }

           // synchronized(ValleyLibraryJsonUnitTest.monitor) {                 // 2
           //     ValleyLibraryJsonUnitTest.monitor.notifyAll();                  // 3
           // }
            // Deliver a normal response or error, depending.
            if (mResponse.isSuccess()) {
                mRequest.deliverResponse(mResponse.result);
            } else {
                mRequest.deliverError(mResponse.error);
            }

            // If this is an intermediate response, add a marker, otherwise we're done
            // and the request can be finished.
            if (mResponse.intermediate) {
                mRequest.addMarker("intermediate-response");
            } else {
                mRequest.finish("done");
            }

            // If we have been provided a post-delivery runnable, run it.
            if (mRunnable != null) {
                mRunnable.run();
            }
        }
    }
}
