package com.mindvalley.android.valleylibrary;


import android.content.Context;
import android.util.Log;
import android.widget.ImageView;

import com.mindvalley.android.valleylibrary.helper.ResultListener;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.io.UnsupportedEncodingException;

import static junit.framework.Assert.fail;
import static org.mockito.Mockito.mock;

/**
 * Junit to Test to check the basic Client Application Interaction with the Valley Library
 * Created by Gautam on 9/21/2016.
 */

@Config(constants = BuildConfig.class,sdk = 18)
@RunWith(RobolectricGradleTestRunner.class)
public class ValleyLibraryInterfaceUnitTest {

    //sample Valley Object
    private Valley valleyJsonParser;

    //Mock the Application Context
    Context ctx= mock(Context.class);
    @Before
    public void setUp() throws Exception {
        valleyJsonParser = new Valley(ctx);
        //Set an ImageParser Object
        ImageParser imageParser = mock(ImageParser.class);
        valleyJsonParser.setParser(imageParser);
    }


    //Test for a failed Image Parser Call
    //A JsonUrl Request is send whereas the Set parser is an ImageParser
    @Test
    public void checkImageParserFailureCall() throws UnsupportedEncodingException {

        final String tag_json_arry = "json_array_req";
        final String url = "http://pastebin.com/raw/wgkJgazE";

        //this call is bound to fail since the valley object is set to ImageParser Instance
        try
        {
            valleyJsonParser.parseDoc(url, tag_json_arry, new ResultListener() {
                public void executed(String _jsonString, Boolean success) {
                    if (success) {
                        success = false;
                    }

                }
            });
            fail("expected exception due to invalid object call in Method");
        }
        catch (IllegalArgumentException success) {
            Log.e("ErrorInTest",success.getMessage());

        }
    }

    //Test for a failed Json Parser Call
    //A ImageParsing  Request is send whereas the Set parser is an JsonParser
    @Test
    public void checkJsonParserFailureCall()
    {

        DocParser jsonDocParser = mock( DocParser.class);
        valleyJsonParser.setParser(jsonDocParser);
        ImageView image = mock(ImageView.class);
        String tag_json_arry = "json_array_req";
        String url = "http://pastebin.com/raw/wgkJgazE";
        try
        {
            valleyJsonParser.loadImage(url,R.drawable.placeholdersample,image);
            fail("expected exception due to invalid object call in Method");
        }
        catch (IllegalArgumentException success) {
            Log.e("ErrorInTest",success.getMessage());

        }


    }



}
