package com.mindvalley.android.valleylibrary.MockClasses;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;

/**
 * Created by Gautam on 9/18/2016.
 */
public class MockJsonArrayRequest extends MockJsonRequest<MockJSONArray> {

    /**
     * Creates a new request.
     * @param url URL to fetch the JSON from
     * @param listener Listener to receive the JSON response
     * @param errorListener Error listener, or null to ignore errors.
     */
    public MockJsonArrayRequest(String url, MockResponse.Listener<MockJSONArray> listener, MockResponse.ErrorListener errorListener) {
        super(Method.GET, url, null,listener, errorListener);
    }

    /**
     * Creates a new request.
     * @param method the HTTP method to use
     * @param url URL to fetch the JSON from
     * @param jsonRequest A {@link JSONArray} to post with the request. Null is allowed and
     *   indicates no parameters will be posted along with request.
     * @param listener Listener to receive the JSON response
     * @param errorListener Error listener, or null to ignore errors.
     */
    public MockJsonArrayRequest(int method, String url, MockJSONArray jsonRequest,
                            MockResponse.Listener<MockJSONArray> listener, MockResponse.ErrorListener errorListener) {
        super(method, url, (jsonRequest == null) ? null : jsonRequest.toString(), listener,
                errorListener);
    }

    @Override
    protected MockResponse<MockJSONArray> parseNetworkResponse(MockNetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    MockHttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
            return MockResponse.success(new MockJSONArray(jsonString),
                    MockHttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return MockResponse.error(new MockParseError(e));
        } catch (JSONException je) {
            return MockResponse.error(new MockParseError(je));
        }
    }
}

