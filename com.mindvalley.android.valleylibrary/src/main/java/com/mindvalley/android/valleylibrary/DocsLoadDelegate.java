package com.mindvalley.android.valleylibrary;
import com.mindvalley.android.valleylibrary.helper.ResultListener;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
/**
 * An Interface which provides the common functions required by the different_
 * Document parsing subclasses which carry out the Actual Parsing tasks
 * @author Gautam on 9/20/2016.
 */
public interface DocsLoadDelegate {

    /**
     * Functions which carries out the actual parsing tasks
     */
    public List<HashMap<String, Object>> parseDocs(String url);
    /**
     * Functions which carries out the actual parsing tasks
     * @url url the document url
     * @tag tag for unique identification of the client requests
     * @url listener to inform the calling class about success or failure status after executing the requests
     */
    public void parseDocs(String url, String tag,ResultListener listener) throws UnsupportedEncodingException;
}
