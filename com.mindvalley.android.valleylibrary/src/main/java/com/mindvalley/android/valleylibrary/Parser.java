package com.mindvalley.android.valleylibrary;

import android.content.Context;
import android.widget.ImageView;


/**
 * An Abstract Base Class which delegates the task of
 * parsing an object to its SubClasses
 * @author Gautam on 9/20/2016.
 */

public abstract class Parser {


    //Interface Declaration to instantiate the required Actual Parser Executor Objects
    ImageLoadDelegate imageLoadDelegate;
    DocsLoadDelegate docsLoadDelegate;

    Context context=null;



    /**
     * Default Constructor
     */
    public Parser()
    {

    }

    /**
     * Constructs a Parser subclass
     *
     * @param context context of the application
     */
    public Parser(Context context)
    {
        if (context == null)
        {
            throw new IllegalArgumentException("Context must not be null.");
        }
        this.context = context.getApplicationContext();
    }


    /**
     * Constructs a Parser subclass
     *
     * @param url Image url
     * @param imagePlaceHolder Placeholder Image id which is displayed incase of error
     * @param imagePlaceHolder View where Image will be displayed
     */
    public void parseImage(String url,int imagePlaceHolder,ImageView imageView)
    {

        imageLoadDelegate.loadImage(url,imagePlaceHolder,imageView);

    }

    /**
     * Constructs a Parser subclass
     *
     * @param url Document url from which data will be parsed
     */
    public void parseDocs(String url)
    {

        docsLoadDelegate.parseDocs(url);

    }


    public void reloadImage(ImageView imgView)
    {
        imageLoadDelegate.reloadImage(imgView);
    }

}




