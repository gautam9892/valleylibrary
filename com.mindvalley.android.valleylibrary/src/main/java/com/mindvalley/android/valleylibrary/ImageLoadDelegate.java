package com.mindvalley.android.valleylibrary;

import android.widget.ImageView;
/**
 * An Interface which provides the common functions required by the different_
 * Image parsing subclasses which carry out the Actual Parsing tasks
 * @author Gautam on 9/20/2016.
 */
public interface ImageLoadDelegate {

    /**
     * Function to Load an Image in an Imageview
     *
     * @param url Image url
     * @param imagePlaceHolder Placeholder Image id which is displayed incase of error
     * @param imagePlaceHolder View where Image will be displayed
     */
    public void loadImage(String url,int imagePlaceHolder,ImageView imgview);
    public void reloadImage(ImageView imgView);

}
