package com.mindvalley.android.valleylibrary;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.JsonParser;
import com.mindvalley.android.valleylibrary.helper.ResultListener;

import org.json.JSONArray;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
/**
 * An SubClass which extends from Delegate Interface
 * This class carries out the actual task of Parsing the Documents
 * @author Gautam on 9/20/2016.
 */
public class JsonParsingExecutor implements DocsLoadDelegate
{
    Context ctx;
    final ProgressDialog pDialog;
    public static String TAG = "";
    static JsonParser parser = new JsonParser();
    List<HashMap<String, Object>> listJson = new ArrayList<>();
    public List<HashMap<String, Object>> getListJson() {
        return listJson;
    }

    public void setListJson(List<HashMap<String, Object>> listJson) {
        this.listJson = listJson;
    }

    Boolean status=false;
    public JsonParsingExecutor(Context ctx) {

        this.ctx = ctx;
        pDialog = new ProgressDialog(ctx);
        TAG = ctx.getClass().getSimpleName();
    }

    public interface JsonDocRecieved {
        public void sendJsonDocAsMap(List<HashMap<String, Object>> list);
    }




    /**
     * Creates a new JsonArrayrequest.
     * @param url URL to fetch the JSON from
     */
    private JsonArrayRequest generateJsonArrayRequest(String url)
    {


        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(JsonParsingExecutor.TAG, response.toString());
                        status=true;
                        listener.executed(response.toString(),status);
                        /*try {

                            for(int i = 0; i < response.length() ;i++) {

                                listJson.add(readJsonResponseObject(response, null,i));
                            }
                            listener.executed(listJson);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }*/
                        pDialog.hide();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(JsonParsingExecutor.TAG, "Error: " + error.getMessage());
                status=false;
                listener.executed("",status);
                pDialog.hide();
            }
        });

        return req;
    }

    @Override
    public List<HashMap<String, Object>> parseDocs(String url) {

        // Adding request to request queue
        AppController.getInstance(ctx).addToRequestQueue(generateJsonArrayRequest(url),"");
        return getListJson();

    }

    //Listener to Inform the Client Activity about the Status of the Request
    ResultListener listener;
    @Override
    public void parseDocs(String url, String tag,ResultListener listener) throws UnsupportedEncodingException {
        // Adding request to request queue
        this.listener = listener;

        //Check the RequestQueue to see if the Url is Cached
        Cache cache = AppController.getInstance(ctx).getRequestQueue().getCache();
        Cache.Entry entry = cache.get(url);
        if(entry != null){
            try {
                //Cached found
                String data = new String(entry.data, "UTF-8");
                status=true;
                //Send the cached data to the client
                listener.executed(data.toString(),status);
                // handle data, like converting it to xml, json, bitmap etc.,
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        else{

            // Cached response doesn't exists
            //Add the Request to the Request Queue
            AppController.getInstance(ctx).addToRequestQueue(generateJsonArrayRequest(url),tag);

        }






    }

    /**
     * Function's to read the Response Json string and Return an Map
     */
   /* public Boolean singleParent = false;
    public Boolean getIn(String response,Boolean isObject)
    {
        int objectIndex = -1;
        int arrayIndex = -1;

        if(isObject) {
            objectIndex = response.indexOf('{', 0);
            arrayIndex = response.indexOf('[', 1);

            if ((objectIndex == 0) && (arrayIndex == -1)) {

                singleParent = true;
                return true;
            }
        }
        else
        {
            objectIndex = response.indexOf('{',1);
            arrayIndex = response.indexOf('[',0);

            if ((arrayIndex < objectIndex) && (objectIndex != -1)) {
                singleParent = false;
                return true;


            }

        }
        return (objectIndex < arrayIndex);
    }
    private HashMap<String, Object> readJsonResponseObject(JSONArray _responseArray, JSONObject _responseObject, int count) throws JSONException {

        //JSONArray arrayObject = (JSONArray) response.getJSONArray(index);
        JSONObject jsonObject = null;
        JSONArray responseArry=null;
        JSONObject responseObj=null;
        /*if (_response instanceof JSONArray) {
            responseArry = (JSONArray) _response;
        }
        if (_response instanceof JSONObject){
            responseObj = (JSONObject) _response;
        }

        if (_responseArray != null) {

            if(getIn(_responseArray.toString(),false) && (!singleParent))
            {
                jsonObject = (JSONObject)_responseArray.getJSONObject(count);
            }

        }
        else if(_responseObject != null)
        {
            if(getIn(_responseObject.toString(),true) && singleParent)
            {
                jsonObject = (JSONObject)_responseObject;
            }
        }
        // Boolean isObj = getIn(_response.toString(),true);
        if (isObj)
        {
             if (responseArry == null)
             {
                 jsonObject = responseObj;

             }
             else
             jsonObject = (JSONObject)responseArry.getJSONObject(0);
        }
        else
        {

        }



        //   JSONObject object = (JSONObject)response.getJSONObject(i);
        //Set<Map.Entry<String, JsonElement>> set = object.;
        //Iterator<Map.Entry<String, JsonElement>> iterator = set.iterator();
        Iterator<String> iterator = jsonObject.keys();
        HashMap<String, Object> map = new HashMap<String, Object>();
        while (iterator.hasNext()) {

            // Map.Entry<String, JsonElement> entry = iterator.next();
            String Key = iterator.next();
            Object obj = jsonObject.get(Key);


                String key = entry.getKey();
                JsonElement value = entry.getValue();

            if (null != obj) {

                if (obj instanceof JSONObject) {

                    map.put(Key,readJsonResponseObject(null,(JSONObject)obj,0));
                } else if ((obj instanceof JSONArray) && obj.toString().contains(":")) {

                    List<HashMap<String, Object>> list = new ArrayList<>();
                    for(int i=0;i<(((JSONArray)obj).length());i++) {

                        list.add(readJsonResponseObject((JSONArray) obj, null,i));

                    }
                    map.put(Key, list);
                    //JSONArray array = ((JSONArray) obj).getJSONArray(0);
                    //if (null != array) {
                                /*for (JSONArray ele: array) {
                                    list.add(readJsonResponseObject(element.toString()));
                                }
                    //  map.put(Key, list);
                    //}
                } else if ((obj instanceof JSONArray) && !obj.toString().contains(":")) {
                    if(((JSONArray) obj).length() == 0)
                    {
                        map.put(Key,"");
                    }
                    else
                        map.put(Key,((JSONArray) obj).getJSONArray(0));
                }
                else {
                    map.put(Key,obj.toString());
                }
            }
        }
        return map;
    }*/
/*
    if (null != value) {
        if (!value.isJsonPrimitive()) {
            if (value.isJsonObject()) {

                map.put(key, readJsonResponseObject(value.toString()));
            } else if (value.isJsonArray() && value.toString().contains(":")) {

                List<HashMap<String, Object>> list = new ArrayList<>();
                JsonArray array = value.getAsJsonArray();
                if (null != array) {
                    for (JsonElement element : array) {
                        list.add(readJsonResponseObject(element.toString()));
                    }
                    map.put(key, list);
                }
            } else if (value.isJsonArray() && !value.toString().contains(":")) {
                map.put(key, value.getAsJsonArray());
            }
        } else {
            map.put(key, value.getAsString());
        }
    }*/
}

