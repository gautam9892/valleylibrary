package com.mindvalley.android.valleylibrary;
import android.content.Context;
/**
 * An SubClass which extends from Abstract Parent Parser Class
 * This Class instantiates an ImageLoadDelegate Object which is delegated the Actual Task of parsing the Images
 * @author Gautam on 9/20/2016.
 */
public class ImageParser extends Parser{


    public  ImageParser() {

        super();

    }

    /**
     *
     * Function to create a Instance of Parser Subclass
     * Also instantiates an ImageLoadDelegate Object which is delegated the Actual Task of parsing the Images
     * @param ctx of the Calling Application.
     */
    public  ImageParser(Context ctx) {

        super(ctx);
        imageLoadDelegate = new ImageParsingExecutor(ctx);

    }








}

