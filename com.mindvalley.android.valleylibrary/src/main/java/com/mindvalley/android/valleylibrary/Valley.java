package com.mindvalley.android.valleylibrary;

import android.content.Context;
import android.widget.ImageView;

import com.mindvalley.android.valleylibrary.helper.ResultListener;

import java.io.UnsupportedEncodingException;

/**
 * Main Library class containing the Interface to interact with the ValleyLibrary
 * Client can instantiate this class in Application to interact with the library
 * @author Gautam on 9/20/2016
 */
public class Valley {


    // static volatile Valley singleton = null;
    final Context context;

    //Default to Image Parser
    private Parser parser = new ImageParser();

    /*public static Valley FactoryValley(Context context) {
        if (singleton == null) {
            synchronized (Valley.class) {
                if (singleton == null) {
                    singleton = new Builder(context).build();
                }
            }
        }
        return singleton;
    }

    public static class Builder
    {
        private final Context context;
        public Builder(Context context) {
            if (context == null) {
                throw new IllegalArgumentException("Context must not be null.");
            }
            this.context = context.getApplicationContext();
        }

        public Valley build() {

            return new Valley(context);

        }
    }*/

    /**
     * Constructs a Valley Object
     *
     * @param ctx context of the application
     */
    public Valley(Context ctx)
    {
        if (ctx == null)
        {
            throw new IllegalArgumentException("Context must not be null.");
        }
        this.context = ctx.getApplicationContext();
    }

    /**
     * Function to set the required parser subclass as per the Client Needs
     *
     * @param parser Parser subclasses as per the requirements of the User
     */

    public  void setParser(Parser parser)
    {
        if (parser == null)
        {
            throw new IllegalArgumentException("Parser Object must not be null.");
        }
        this.parser = parser;

    }

    /**
     * Function to return the required parser object set through the function #setParser
     *
     * @return  Parser subclasse object set through the function #setParser
     */
    private Parser getParser()
    {

        return this.parser;

    }




    /**
     * Forwards the actual image parsing task to the concerned delegate
     *
     * @param url Image url
     * @param imagePlaceHolder Placeholder Image id which is displayed incase of error
     * @param imagePlaceHolder View where Image will be displayed
     */
    public void loadImage(String url,int imagePlaceHolder,ImageView imageView)
    {
        if(this.getParser().imageLoadDelegate == null) {
            throw new IllegalArgumentException("ImageLoaderObject is Null");
        }

        this.getParser().parseImage(url, imagePlaceHolder, imageView);
    }

    public void parseDoc(String url)
    {
        this.getParser().docsLoadDelegate.parseDocs(url);
    }

    /**
     * Forwards the actual  Document parsing task to the concerned delegate
     *
     * @param url Image url
     * @param tag tag to identify the Request
     * @param listener to inform the calling client application about request status
     */

    public void parseDoc(String url, String tag,ResultListener listener) throws UnsupportedEncodingException {

        if(this.getParser().docsLoadDelegate == null)
        {
            throw new IllegalArgumentException("JsonLoaderObject is Null");
        }

        this.getParser().docsLoadDelegate.parseDocs(url,tag,listener);

    }




}

