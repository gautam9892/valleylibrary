package com.mindvalley.android.valleylibrary.helper;

/**
 * Callback interface used by the library for informing the client application about request status
 * Created by Gautam on 9/20/2016.
 */
public interface ResultListener {


        public void executed(String JsonString,Boolean status);


}
