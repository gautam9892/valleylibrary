package com.mindvalley.android.valleylibrary;

import android.content.Context;

import java.util.HashMap;
import java.util.List;
/**
 * A SubClass which extends from Abstract Parent Parser Class
 * This Class instantiates an DocsLoadDelegate Object which is delegated the Actual Task of parsing the Documents
 * @author Gautam on 9/20/2016.
 */

public class DocParser extends Parser  {

    List<HashMap<String, Object>> jsonDoc;
    public List<HashMap<String, Object>> getJsonDoc() {
        return jsonDoc;
    }

    public void setJsonDoc(List<HashMap<String, Object>> jsonDoc) {
        this.jsonDoc = jsonDoc;
    }


    public DocParser() {

        super();
    }

    /**
     *
     * Function to create a Instance of Parser Subclass
     * Also instantiates an DocsLoadDelegate Object which is delegated the Actual Task of parsing the Docs
     * @param ctx of the Calling Application.
     */
    public DocParser(Context ctx) {

        super(ctx);
        docsLoadDelegate = new JsonParsingExecutor(ctx);
    }




}
