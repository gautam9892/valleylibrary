package com.mindvalley.android.valleylibrary;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.mindvalley.android.valleylibrary.helper.LruBitmapCache;

/**
 * Class to return a Singleton Document Parsing Object
 * client can call factory method #getInstance to get an Instance of this class to Carry out the Required
 * Documents parsing functionalities
 * Created by Gautam on 9/20/2016.
 */
public class AppController {

    public static final String TAG = AppController.class
            .getSimpleName();


    //Queue when the Client Request is Stored
    private RequestQueue mRequestQueue;

    private ImageLoader mImageLoader;

    //Context of the Loading Application
    final Context context;

    //Singleton Instance
    private static volatile AppController mInstance;


    //Create a Singleton Instance
    public static  synchronized AppController getInstance(Context ctx) {
        if (mInstance == null) {
            synchronized (AppController.class) {
                if (mInstance == null) {
                    mInstance = new Builder(ctx).build();
                }
            }
        }
        return mInstance;

    }


    //Static Class to Build the Instance
    public static class Builder
    {
        private final Context context;
        public Builder(Context context) {
            if (context == null) {
                throw new IllegalArgumentException("Context must not be null.");
            }
            this.context = context.getApplicationContext();
        }

        public AppController build() {
            Context context = this.context;
            return new AppController(context);

        }
    }

    private AppController(Context context)
    {
        this.context = context;
    }

    /*public static synchronized AppController getInstance() {
        return mInstance;
    }*/


    /**
     * Function which return the Queue to hold Client Request
     *
     * @Return Request Queue which will be holding the Client Request's
     */
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context);
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }


    /**
     * Function which return the Queue to hold Client Request
     *
     * @Params Req  Client Request Object
     * @tag tag to Identify the Request
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    /**
     * Function which return the Queue to hold Client Request
     *
     * @Params Req  Client Request Object
     */
    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    /**
     * Function which return the Queue to hold Client Request
     *
     * @tag tag to Identify the Request and Cancel it
     */
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
