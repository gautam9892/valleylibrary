package com.mindvalley.android.valleylibrary;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
/**
 * An SubClass which extends from Delegate Interface
 * This class carries out the actual task of Parsing the Image
 * @author Gautam on 9/20/2016.
 */
public class ImageParsingExecutor implements ImageLoadDelegate {

    //context of the calling Application
    Context ctx;
    static int a=0;
    public ImageParsingExecutor(Context ctx) {

        this.ctx = ctx;

    }


    public void reloadImage(ImageView imgView)
    {
        Picasso.with(ctx).cancelRequest(imgView);
    }
    @Override
    public void loadImage(String url, int imagePlaceHolder, ImageView imgview) {

        a++;
        //Accessing Picasso Library for Image Load
        Picasso.with(ctx).load(url)
                .error(imagePlaceHolder)
                .fit()
                .placeholder(imagePlaceHolder)
                .into(imgview);

        //Intentional to Let the User load Images by Click on the ImageView
        if (a == 1 || a == 2 || a == 3)
        {
            Picasso.with(ctx).cancelRequest(imgview);
        }


    }
}

