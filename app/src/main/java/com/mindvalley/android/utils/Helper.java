package com.mindvalley.android.utils;

/**
 * Contains Basic Helper Functions to be used throughout the Application
 * Created by Gautam on 9/21/2016.
 */
public class Helper {

    /**
     * Function which accepts a string and Return a Representable formatted String
     *
     * @param stringToBuild string to Parser
     * @return string Formatted String
     */

    public static String parseString(String stringToBuild)
    {
        String[] stringMessageArray = stringToBuild.split("-");
        StringBuilder sb = new StringBuilder();

        for(int i =0;i < stringMessageArray.length;i++) {
            sb.append(stringMessageArray[i]);
            sb.append("\n\n");
        }
        return sb.toString();
    }
}
