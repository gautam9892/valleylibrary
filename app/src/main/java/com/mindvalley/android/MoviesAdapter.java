package com.mindvalley.android;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindvalley.android.models.Movie;

import java.util.List;
/**
 * An Class Used by the Recycler View to display Movie Objects passed by the Parent Activity
 * @author Gautam on 9/20/2016.
 */

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder> {

    private List<Movie> moviesList;
    private Context mContext;
    //Listener to Parent Activity to load Images
    private ImageLoadListener listener;

    /**
      Inner class to Hold the Views of the Individual rows of Recycler View
     */

    public class MyViewHolder extends RecyclerView.ViewHolder {
        protected TextView title, year, genre;
        protected ImageView imageView;
        public MyViewHolder(View view) {
            super(view);
            this.imageView = (ImageView) view.findViewById(R.id.thumbnail);
            this.title = (TextView) view.findViewById(R.id.title);
            this.genre = (TextView) view.findViewById(R.id.genre);
            this.year = (TextView) view.findViewById(R.id.year);
        }
    }


    /**
     * Interface to Callback parent activity to carry out different task like ImageLoad,Reload
     */

    public interface ImageLoadListener {
         void onLoadImage(String link,int placeHolderId,ImageView ImageView);
         void reload(ImageView v);
    }


    /**
     * Constructs a MovieAdapter Object
     *
     * @param context context of the parent Activity
     * @param moviesList list of all the movie objects
     */

    public MoviesAdapter(Context context, List<Movie> moviesList) {
        this.moviesList = moviesList;
        this.mContext = context;
        if (this.mContext instanceof ImageLoadListener) {
            listener = (ImageLoadListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implemenet MoviesAdapter.OnItemSelectedListener");
        }
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_row, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder,final int position) {
        Movie movie = moviesList.get(position);
        holder.title.setText(movie.getTitle());
        holder.genre.setText(movie.getGenre());
        holder.year.setText(movie.getYear());

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView imgView = (ImageView)v.findViewById(R.id.thumbnail);
                listener.onLoadImage(moviesList.get(position).getThumbnail().toString(),R.drawable.placeholder,imgView);
            }
        });
        listener.onLoadImage(movie.getThumbnail().toString(),R.drawable.placeholder,holder.imageView);

    }



    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
