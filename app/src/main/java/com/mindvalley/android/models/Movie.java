package com.mindvalley.android.models;

/**
 * An Class to representing the Movie Objects
 * @author Gautam on 9/20/2016.
 */
public class Movie {

    private String title, genre, year;
    private String thumbnail;

    public Movie() {
    }
    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
    public Movie(String title, String genre, String year,String thumbnail) {
        this.title = title;
        this.genre = genre;
        this.year = year;
        this.thumbnail = thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}

