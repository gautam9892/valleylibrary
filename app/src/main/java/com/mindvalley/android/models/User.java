package com.mindvalley.android.models;
import java.util.HashMap;
import java.util.Map;
/**
 * An Class representing the Users
 * @author Gautam on 9/20/2016.
 */

public class User {

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getProfileImages() {
        return profileImages;
    }

    public void setProfileImages(Map<String, String> profileImages) {
        this.profileImages = profileImages;
    }

    public Map<String, String> getLinks() {
        return links;
    }

    public void setLinks(Map<String, String> links) {
        this.links = links;
    }

    String id,username,name;
    Map<String,String> profileImages= new HashMap<String,String>();
    Map<String,String> links= new HashMap<String,String>();

}

