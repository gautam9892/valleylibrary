package com.mindvalley.android.models;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * An Class representing the PinBoard Objects
 * @author Gautam on 9/20/2016.
 */

public class PinBoard {

    String id;
    String created_at;
    String width;
    String height;
    String color;
    String likes;
    String liked_by_user;

    public String getCurrent_user_collections() {
        return current_user_collections;
    }

    public void setCurrent_user_collections(String current_user_collections) {
        this.current_user_collections = current_user_collections;
    }

    String current_user_collections;
    Map<String,String> urls = new HashMap<String,String>();
    Map<String,String> links= new HashMap<String,String>();
    List<Categories> categories= new ArrayList<Categories>();
    User user=new User();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getLiked_by_user() {
        return liked_by_user;
    }

    public void setLiked_by_user(String liked_by_user) {
        this.liked_by_user = liked_by_user;
    }

    public Map<String, String> getUrls() {
        return urls;
    }

    public void setUrls(Map<String, String> urls) {
        this.urls = urls;
    }

    public Map<String, String> getLinks() {
        return links;
    }

    public void setLinks(Map<String, String> links) {
        this.links = links;
    }

    public List<Categories> getCategories() {
        return categories;
    }

    public void setCategories(List<Categories> categories) {
        this.categories = categories;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }



}

