package com.mindvalley.android.models;
import java.util.HashMap;
import java.util.Map;
/**
 * An Class to representing the Categories inside a PinBoard
 * @author Gautam on 9/20/2016.
 */

public class Categories {

    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoto_count() {
        return photo_count;
    }

    public void setPhoto_count(String photo_count) {
        this.photo_count = photo_count;
    }

    public Map<String, String> getLinks() {
        return links;
    }

    public void setLinks(Map<String, String> links) {
        this.links = links;
    }

    String title;
    String photo_count;
    Map<String,String> links= new HashMap<String,String>();

}

