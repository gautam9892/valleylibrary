package com.mindvalley.android;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mindvalley.android.models.PinBoard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
/**
 * An Class Used by the Recycler View to display Pinboard Objects passed by the Parent Activity
 * @author Gautam on 9/20/2016.
 */
public class PinBoardAdapter extends RecyclerView.Adapter<PinBoardAdapter.MyViewHolder>  {

    private List<PinBoard> pinBoardList;
    private Context mContext;
    private ImageLoadListener listener;

    // To create the Links
    List<SpannableString> boardurls;

    /**
     Inner class to Hold the Views of the Individual rows of Recycler View
     */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        protected TextView created_at,likes,likedByUser,urlView;
        protected TextView rawUrl,fullUrl,regularUrl,smallUrl,thumbUrl;
        protected ImageView imageView,profileImage;
        protected TextView userName,userProfileImageL;
        protected RelativeLayout pinBoard;
        public MyViewHolder(View view) {
            super(view);
            this.pinBoard = (RelativeLayout) view.findViewById(R.id.userPinBoard);
            this.created_at = (TextView) view.findViewById(R.id.created_at);
            this.likes = (TextView) view.findViewById(R.id.likes);
            this.likedByUser = (TextView) view.findViewById(R.id.likedByUser);
            this.imageView = (ImageView)view.findViewById(R.id.thumbnailU);
            this.rawUrl = (TextView) view.findViewById(R.id.urlRaw);
            this.fullUrl = (TextView) view.findViewById(R.id.urlFull);
            this.regularUrl = (TextView) view.findViewById(R.id.urlRegular);
            this.smallUrl = (TextView) view.findViewById(R.id.urlSmall);
            this.thumbUrl = (TextView) view.findViewById(R.id.urlThumb);

            //fetch user controls
            this.userName = (TextView) view.findViewById(R.id.userName);
            this.profileImage = (ImageView)view.findViewById(R.id.profileImage);
            this.userProfileImageL = (TextView) view.findViewById(R.id.userProfileLarge);


        }
    }

    /**
     * Interface to Callback parent activity to carry out different task like ImageLoad,Reload
     */

    public interface ImageLoadListener {
        public void onLoadImage(String link,int placeHolderId,ImageView ImageView);
    }


    /**
     * Constructs a PinBoard Object
     *
     * @param context context of the parent Activity
     * @param pinBoardList list of all the Pinboard objects
     */
    public PinBoardAdapter(Context context, List<PinBoard> pinBoardList) {
        this.pinBoardList = pinBoardList;
        this.mContext = context;

        if (this.mContext instanceof ImageLoadListener) {
            listener = (ImageLoadListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implemenet MoviesAdapter.OnItemSelectedListener");
        }
    }

    /**
     *  Called to convert the Text to link of the Object passed in as a Argument
     *
     * @param pinboard object containing the text to be converted to links
     */

    public void createSpanUrl(PinBoard pinboard) {

        SpannableString url;
        if (boardurls == null)
        {
            boardurls = new ArrayList<SpannableString>(0);
            HashMap<String,String> urls = (HashMap<String,String>)pinboard.getUrls();


            for(String key:urls.keySet())
            {
                url = new SpannableString(key);
                url.setSpan(new UnderlineSpan(), 0,url.length(), 0);
                boardurls.add(url);
                url = new SpannableString(key);
                url.setSpan(new UnderlineSpan(), 0,url.length(), 0);

            }


        }
        else
            return;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pin_board_row, parent, false);

        return new MyViewHolder(itemView);
    }




    @Override
    public void onBindViewHolder(final MyViewHolder holder,final int position) {



        PinBoard pinBoard = pinBoardList.get(position);
        // holder.pinBoard.setBackgroundColor(Color.parseColor(pinBoard.getColor()));
        holder.created_at.setText("Created: " + pinBoard.getCreated_at());
        holder.likes.setText("Likes: " + pinBoard.getLikes());





        holder.rawUrl.setText(boardurls.get(0));

        holder.thumbUrl .setText(boardurls.get(1));
        holder.smallUrl.setText(boardurls.get(2));
        holder.regularUrl.setText(boardurls.get(3));
        holder.fullUrl.setText(boardurls.get(4));



        if (pinBoard.getLiked_by_user().toString().equals("true")) {
            holder.likedByUser.setText("Liked by User: " + "Yes");
        }
        else
        {
            holder.likedByUser.setText("Liked by User: " + "No");
        }
        //holder.imageView.set(pinBoard.getUrls().get("raw"));

        //holder.links.setText(movie.getYear());

        holder.rawUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext,"Loading Raw..",Toast.LENGTH_SHORT).show();
                holder.rawUrl.setTextColor(Color.parseColor("#FF0000"));
                holder.thumbUrl.setTextColor(Color.parseColor("#3F51B5"));
                holder.regularUrl.setTextColor(Color.parseColor("#3F51B5"));
                holder.fullUrl.setTextColor(Color.parseColor("#3F51B5"));
                holder.smallUrl.setTextColor(Color.parseColor("#3F51B5"));
                listener.onLoadImage(pinBoardList.get(position).getUrls().get("raw"),R.drawable.placeholderpinboard,holder.imageView);
            }
        });
        holder.thumbUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext,"Loading Thumb...",Toast.LENGTH_SHORT).show();
                holder.thumbUrl.setTextColor(Color.parseColor("#FF0000"));
                holder.rawUrl.setTextColor(Color.parseColor("#3F51B5"));
                holder.regularUrl.setTextColor(Color.parseColor("#3F51B5"));
                holder.fullUrl.setTextColor(Color.parseColor("#3F51B5"));
                holder.smallUrl.setTextColor(Color.parseColor("#3F51B5"));



                listener.onLoadImage(pinBoardList.get(position).getUrls().get("thumb"),R.drawable.placeholderpinboard,holder.imageView);
            }
        });
        holder.regularUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext,"Loading Regular...",Toast.LENGTH_SHORT).show();
                holder.regularUrl.setTextColor(Color.parseColor("#FF0000"));
                holder.fullUrl.setTextColor(Color.parseColor("#3F51B5"));
                holder.smallUrl.setTextColor(Color.parseColor("#3F51B5"));
                holder.thumbUrl.setTextColor(Color.parseColor("#3F51B5"));
                holder.rawUrl.setTextColor(Color.parseColor("#3F51B5"));
                listener.onLoadImage(pinBoardList.get(position).getUrls().get("regular"),R.drawable.placeholderpinboard,holder.imageView);
            }
        });
        holder.fullUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext,"Loading Full...",Toast.LENGTH_SHORT).show();
                holder.fullUrl.setTextColor(Color.parseColor("#FF0000"));
                holder.smallUrl.setTextColor(Color.parseColor("#3F51B5"));
                holder.thumbUrl.setTextColor(Color.parseColor("#3F51B5"));
                holder.rawUrl.setTextColor(Color.parseColor("#3F51B5"));
                holder.regularUrl.setTextColor(Color.parseColor("#3F51B5"));

                listener.onLoadImage(pinBoardList.get(position).getUrls().get("full"),R.drawable.placeholderpinboard,holder.imageView);
            }
        });
        holder.smallUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.smallUrl.setTextColor(Color.parseColor("#FF0000"));
                holder.thumbUrl.setTextColor(Color.parseColor("#3F51B5"));
                holder.rawUrl.setTextColor(Color.parseColor("#3F51B5"));
                holder.regularUrl.setTextColor(Color.parseColor("#3F51B5"));
                holder.fullUrl.setTextColor(Color.parseColor("#3F51B5"));

                //Toast.makeText(mContext,"Loading Regular...",Toast.LENGTH_SHORT).show();
                listener.onLoadImage(pinBoardList.get(position).getUrls().get("small"),R.drawable.placeholderpinboard,holder.imageView);
            }
        });

        listener.onLoadImage(pinBoard.getUrls().get("thumb").toString(),R.drawable.placeholderpinboard,holder.imageView);


        //load user details

        holder.userName.setText(pinBoard.getUser().getName().toString().replace("\"", ""));
        holder.userProfileImageL.setText("CLICK ME TO VIEW LARGE");
        holder.userProfileImageL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Toast.makeText(mContext,"Loading Large...",Toast.LENGTH_SHORT).show();
                listener.onLoadImage(pinBoardList.get(position).getUser().getProfileImages().get("large").toString(),R.drawable.placeholderpinboard,holder.profileImage);
            }
        });
        listener.onLoadImage(pinBoard.getUser().getProfileImages().get("medium").toString(),R.drawable.placeholderpinboard,holder.profileImage);
    }



    @Override
    public int getItemCount() {
        if ((pinBoardList.size() > 0)&& (boardurls == null))
        {
            createSpanUrl(pinBoardList.get(0));
        }
        return pinBoardList.size();
    }
}

