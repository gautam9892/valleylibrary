package com.mindvalley.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mindvalley.android.models.Movie;
import com.mindvalley.android.utils.DividerItemDecoration;
import com.mindvalley.android.utils.Helper;
import com.mindvalley.android.utils.RecyclerTouchListener;
import com.mindvalley.android.valleylibrary.ImageParser;
import com.mindvalley.android.valleylibrary.Valley;

import java.util.ArrayList;
import java.util.List;

/**
 * An Activity to Load the PinBoard Objects
 * @author Gautam on 9/20/2016.
 */
public class StartUpActivity extends AppCompatActivity implements MoviesAdapter.ImageLoadListener {

    private static final int USERDETAILSACTIVITY = 1;
    private List<Movie> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;
    private ProgressBar progressBar;

    // Instance of the Valley Library
    private Valley imageParser;
    private Button userInstructionsButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_up);

        /**
         * Create an ValleyLibrary Object to Parser Images
         */
        imageParser = new Valley(StartUpActivity.this);
        imageParser.setParser(new ImageParser(StartUpActivity.this));


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        userInstructionsButton = (Button) findViewById(R.id.buttonUserInstructions);
        mAdapter = new MoviesAdapter(StartUpActivity.this,movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new StartUpActivity.ClickListener() {

            @Override
            public void onClick(View view, int position) {
                Movie movie = movieList.get(position);
                //Intent intent = new Intent(MainActivity.this,PinBoardActivity.class);
                //startActivity(intent);
                Toast.makeText(getApplicationContext(), movie.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {
                try {
                    Movie movie = movieList.get(position);
                    Intent intent = new Intent(StartUpActivity.this,PinBoardActivity.class);
                    startActivityForResult(intent,USERDETAILSACTIVITY);
                    Toast.makeText(getApplicationContext(), movie.getYear() + " is selected!", Toast.LENGTH_SHORT).show();
                }
                catch(Exception e)
                {
                    Log.e("MainActivity",e.getMessage());
                }
            }
        }));

        //function to Read Movie Objects
        prepareMovieData();

        //add a listener to the ButtonClick
        addListenerOnButton();
    }


    @Override
    public void onActivityResult(int requestCode,
                                 int resultCode,
                                 Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (USERDETAILSACTIVITY): {
                if (resultCode == Activity.RESULT_OK) {

                }
                break;
            }

        }
    }

    /**
     * Called when User Clicks the User Instructions Button on the Activity
     */
    public void addListenerOnButton() {
        Resources userInstructionsMessage = getResources();
        StringBuilder sb=new StringBuilder();
        final String title = "UserInstructions";

        String stringIn = userInstructionsMessage.getString(R.string.user_instructions_message);
        final String message = Helper.parseString(stringIn);

        final String okButton = "Ok";
        final String closeButton = "Close";


        userInstructionsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {


                AlertDialog.Builder ad = new AlertDialog.Builder(StartUpActivity.this);
                ad.setTitle(title);
                ad.setMessage(message);
                ad.setPositiveButton(okButton,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int arg1) {

                            }
                        });
                ad.setNegativeButton(closeButton,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int arg1) {
// do nothing
                            }
                        });
                ad.setCancelable(true);
                ad.setOnCancelListener(new DialogInterface.OnCancelListener() {

                    public void onCancel(DialogInterface dialog) {

                    }
                });

                ad.show();

            }
        });
    }



    /*

        public class SampleScrollListener implements AbsListView.OnScrollListener {
            private final Context context;
            private  final Object scrollTag = new Object(); // this can be static or not, depending what u want to achieve

            public SampleScrollListener(Context context) {
                this.context = context;
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                final Picasso picasso = Picasso.with(context);
                if (scrollState == SCROLL_STATE_IDLE || scrollState == SCROLL_STATE_TOUCH_SCROLL) {
                    picasso.resumeTag(scrollTag);
                } else {
                    picasso.pauseTag(scrollTag);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
                // Do nothing.
            }
        }*/

    /**
     * Function to Read MovieData and add them to Listview
     */
    private void prepareMovieData() {
        Movie movie = new Movie("Mad Max:Fury Road", "Action & Adventure", "Long Click Me","https://s-media-cache-ak0.pinimg.com/236x/0a/81/2a/0a812add2780417f604c1ab836140fbd.jpg");
        movieList.add(movie);

        movie = new Movie("Inside Out", "Animation, Kids & Family", "Long Click Me","https://s-media-cache-ak0.pinimg.com/236x/0a/81/2a/0a812add2780417f604c1ab836140fbd.jpg");
        movieList.add(movie);

        movie = new Movie("Star Wars: Episode VII - The Force Awakens", "Action", "Long Click Me","https://s-media-cache-ak0.pinimg.com/236x/0a/81/2a/0a812add2780417f604c1ab836140fbd.jpg");
        movieList.add(movie);

        movie = new Movie("Shaun the Sheep", "Animation", "Long Click Me","http://1.bp.blogspot.com/-d_MbsNAC84g/Tp6QbgZQHMI/AAAAAAAAAWk/XPXp6RBROpU/s1600/250437_197730530273725_196446953735416_555162_1918120_n.jpg");
        movieList.add(movie);

        movie = new Movie("The Martian", "Science Fiction & Fantasy", "Long Click Me","http://onlinegupshup.com/wp-content/uploads/2016/03/Young-Cute-EMO-Boy-Facebook-Profile-Picture-230x230.jpg");
        movieList.add(movie);

        movie = new Movie("Mission: Impossible Rogue Nation", "Action", "Long Click Me","https://s-media-cache-ak0.pinimg.com/236x/e3/2d/60/e32d605de76abb25bd4614265885ca7f.jpg");
        movieList.add(movie);

        movie = new Movie("Up", "Animation", "Long Click Me","http://us.123rf.com/450wm/rimdream/rimdream1209/rimdream120900030/15025338-boy-with-rose-in-his-hand.jpg?ver=6");
        movieList.add(movie);

        movie = new Movie("Star Trek", "Science Fiction", "Long Click Me","http://onlinesocialfun.com/wp-content/uploads/2016/06/13295128_140242356387996_472492936_n-300x300.jpg");
        movieList.add(movie);

        movie = new Movie("The LEGO Movie", "Animation", "Long Click Me","http://us.123rf.com/450wm/rimdream/rimdream1209/rimdream120900030/15025338-boy-with-rose-in-his-hand.jpg?ver=6");
        movieList.add(movie);

        movie = new Movie("The Martian", "Science Fiction & Fantasy", "Long Click Me","http://onlinegupshup.com/wp-content/uploads/2016/03/Young-Cute-EMO-Boy-Facebook-Profile-Picture-230x230.jpg");
        movieList.add(movie);

        movie = new Movie("Mission: Impossible Rogue Nation", "Action", "Long Click Me","https://s-media-cache-ak0.pinimg.com/236x/e3/2d/60/e32d605de76abb25bd4614265885ca7f.jpg");
        movieList.add(movie);

        movie = new Movie("Up", "Animation", "Long Click Me","http://onlinesocialfun.com/wp-content/uploads/2016/06/13295128_140242356387996_472492936_n-300x300.jpg");
        movieList.add(movie);

        movie = new Movie("Star Trek", "Science Fiction", "Long Click Me","http://onlinesocialfun.com/wp-content/uploads/2016/06/13295128_140242356387996_472492936_n-300x300.jpg");
        movieList.add(movie);

        movie = new Movie("The LEGO Movie", "Animation", "Long Click Me","http://us.123rf.com/450wm/rimdream/rimdream1209/rimdream120900030/15025338-boy-with-rose-in-his-hand.jpg?ver=6");
        movieList.add(movie);
/*
        movie = new Movie("Iron Man", "Action & Adventure", "2008");
        movieList.add(movie);

        movie = new Movie("Aliens", "Science Fiction", "1986");
        movieList.add(movie);

        movie = new Movie("Chicken Run", "Animation", "2000");
        movieList.add(movie);

        movie = new Movie("Back to the Future", "Science Fiction", "1985");
        movieList.add(movie);

        movie = new Movie("Raiders of the Lost Ark", "Action & Adventure", "1981");
        movieList.add(movie);

        movie = new Movie("Goldfinger", "Action & Adventure", "1965");
        movieList.add(movie);

        movie = new Movie("Guardians of the Galaxy", "Science Fiction & Fantasy", "2014");
        movieList.add(movie); */

        mAdapter.notifyDataSetChanged();
    }






    @Override
    public void onLoadImage(String link,int placeHolderId,ImageView ImageView) {
        try {
            if (imageParser == null)
            {
                throw new NullPointerException("Valley Object Cannot be Null");
            }
            imageParser.loadImage(link, placeHolderId, ImageView);
        }
        catch(Exception e)
        {
            Log.e("Error",e.getMessage());
        }
    }

    @Override
    public void reload(ImageView v) {
        //valley.reloadImage(v);
    }


    //Interface to Handle Basic Activity onTouc Events
    public interface ClickListener {

        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }
}

