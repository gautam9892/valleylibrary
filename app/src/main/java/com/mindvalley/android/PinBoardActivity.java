package com.mindvalley.android;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mindvalley.android.models.Categories;
import com.mindvalley.android.models.PinBoard;
import com.mindvalley.android.models.User;
import com.mindvalley.android.utils.DividerItemDecoration;
import com.mindvalley.android.utils.Helper;
import com.mindvalley.android.valleylibrary.DocParser;
import com.mindvalley.android.valleylibrary.ImageParser;
import com.mindvalley.android.valleylibrary.Valley;
import com.mindvalley.android.valleylibrary.helper.ResultListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
/**
 * An Activity to Load the Movies Objects
 * @author Gautam on 9/20/2016.
 */
public class PinBoardActivity extends AppCompatActivity implements PinBoardAdapter.ImageLoadListener,SwipeRefreshLayout.OnRefreshListener{


    //Instances of the Valley Library
    private Valley valleyJsonParser;
    private Valley valleyImageParser;

    //To Hold the PinBoard Onjects read from Json Url
    List<PinBoard> pinBoardList;

    //Adapter for Displaying the PinBoard Objects
    private PinBoardAdapter mAdapter;
    public static final String TAG = PinBoardActivity.class
            .getSimpleName();
    String jsonString = "";

    private SwipeRefreshLayout swipeRefreshLayout;
    private GoogleApiClient client;

    //Recycler View to diplay the Parsed data
    private RecyclerView recyclerView;
    final String tag_json_arry = "json_array_req";
    final String url = "http://pastebin.com/raw/wgkJgazE";
    private Button userInstructionsButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_board);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        userInstructionsButton = (Button) findViewById(R.id.buttonUserInstructions);


        pinBoardList = new LinkedList<PinBoard>();

        /**
         * Create an ValleyLibrary Object to Parser Documents
         */
        valleyJsonParser = new Valley(PinBoardActivity.this);
        valleyJsonParser.setParser(new DocParser(PinBoardActivity.this));

        /**
         * Create an ValleyLibrary Object to Parser Images
         */

        valleyImageParser = new Valley(PinBoardActivity.this);
        valleyImageParser.setParser(new ImageParser(PinBoardActivity.this));



        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        mAdapter = new PinBoardAdapter(PinBoardActivity.this, pinBoardList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);


        /**
         * Implement a Post Runnable once the Activity is loaded
         */
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        //Read the Parser json Document
                                        readDoc();

                                    }
                                }
        );

        //add a listener to the ButtonClick
        addListenerOnButton();
    }


    /**
     * Function to Send a Request to Valley Library to Parse a Document
     * A callback ResultListener is also called once the Library Parses the Required Docs
     */
    public void readDoc()
    {
        try {
            valleyJsonParser.parseDoc(url, tag_json_arry, new ResultListener() {
                public void executed(String _jsonString, Boolean success) {
                    if (success) {
                        jsonString = _jsonString;
                        //Called to read the Objects successfully parsed by the Valley library
                        readDoc(jsonString);
                    }
                    swipeRefreshLayout.setRefreshing(false);

                }
            });
        }
        catch(Exception e)
        {
            Log.e("Error Parsing Docs",e.getMessage());
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    // public void readDoc(List<HashMap<String, Object>> list){
    public void readDoc(String jsonString) {

       try {
           PinBoard pinboard;
           new PinBoard();

           JsonElement jelement = new JsonParser().parse(jsonString);
           JsonArray jarray = jelement.getAsJsonArray();

           for (int i = 0; i < jarray.size(); i++) {


               JsonElement userRecord = jarray.get(i);
               JsonObject uRecord = userRecord.getAsJsonObject();

               pinboard = new PinBoard();
               pinboard.setId(uRecord.get("id").toString());
               pinboard.setCreated_at(uRecord.get("created_at").toString().replace("\"", ""));
               pinboard.setWidth(uRecord.get("width").toString());
               pinboard.setHeight(uRecord.get("height").toString());
               pinboard.setColor(uRecord.get("color").toString().replace("\"", ""));
               pinboard.setLikes(uRecord.get("likes").toString());
               pinboard.setLiked_by_user(uRecord.get("liked_by_user").toString());
               JsonArray current_user_collections = uRecord.get("current_user_collections").getAsJsonArray();
               for (JsonElement key : current_user_collections) {


               }
               pinboard.setCurrent_user_collections("");
               Map<String, String> urlsMap = new HashMap<String, String>();


               JsonObject urls = uRecord.getAsJsonObject("urls");
               urlsMap.put("raw", urls.get("raw").toString().replace("\"", ""));
               urlsMap.put("full", urls.get("full").toString().replace("\"", ""));
               urlsMap.put("regular", urls.get("regular").toString().replace("\"", ""));
               urlsMap.put("small", urls.get("small").toString().replace("\"", ""));
               urlsMap.put("thumb", urls.get("thumb").toString().replace("\"", ""));

               pinboard.setUrls(urlsMap);

               JsonObject links = uRecord.getAsJsonObject("links");
               Map<String, String> urlsLinks = new HashMap<String, String>();
               urlsLinks.put("self", links.get("self").toString());
               urlsLinks.put("html", links.get("html").toString());
               urlsLinks.put("download", links.get("download").toString());

               pinboard.setLinks(urlsLinks);

               Categories categoryObject;
               JsonArray categories = uRecord.getAsJsonArray("categories");
               List<Categories> categoryLink = new ArrayList<Categories>();
               for (JsonElement category : categories) {

                   categoryObject = new Categories();
                   JsonObject _category = category.getAsJsonObject();
                   categoryObject.setId(_category.get("id").toString());
                   categoryObject.setTitle(_category.get("title").toString());
                   categoryObject.setPhoto_count(_category.get("photo_count").toString());
                   Map<String, String> cateogryLinks = new HashMap<String, String>();
                   JsonObject userlinks = _category.getAsJsonObject("links");
                   cateogryLinks.put("self", userlinks.get("self").toString());
                   cateogryLinks.put("photos", userlinks.get("photos").toString());

                   categoryObject.setLinks(cateogryLinks);

                   categoryLink.add(categoryObject);
               }
               pinboard.setCategories(categoryLink);

               User userObject = new User();

               JsonObject user = uRecord.getAsJsonObject("user");

               userObject.setId(user.get("id").toString());
               userObject.setUsername((user.get("username").toString()).replace("\"", ""));
               userObject.setName(user.get("name").toString());

               Map<String, String> mapProfileImages = new HashMap<String, String>();

               JsonObject profileImage = user.getAsJsonObject("profile_image");
               mapProfileImages.put("small", profileImage.get("small").toString().replace("\"", ""));
               mapProfileImages.put("medium", profileImage.get("medium").toString().replace("\"", ""));
               mapProfileImages.put("large", profileImage.get("large").toString().replace("\"", ""));

               userObject.setProfileImages(mapProfileImages);

               Map<String, String> mapProfileLinks = new HashMap<String, String>();
               JsonObject profileLinks = user.getAsJsonObject("links");

               mapProfileLinks.put("self", profileLinks.get("self").toString());
               mapProfileLinks.put("html", profileLinks.get("html").toString());
               mapProfileLinks.put("photos", profileLinks.get("photos").toString());
               mapProfileLinks.put("likes", profileLinks.get("likes").toString());

               userObject.setLinks(mapProfileLinks);

               pinboard.setUser(userObject);


               pinBoardList.add(pinboard);


           }
       }
        catch(Exception e)
           {
               Log.e("JsonString-Parsing",e.getMessage());
           }
        mAdapter.notifyDataSetChanged();
    }

    /**
     * Callback fired by Recycler View informing to load the Image
     *
     * @param url Image url
     * @param imagePlaceHolder Placeholder Image id which is displayed incase of error
     * @param imageView View where Image will be displayed
     */
    @Override
    public void onLoadImage(String url, int imagePlaceHolder, ImageView imageView) {

        try {
            if (valleyImageParser == null)
            {
                throw new NullPointerException("Valley Object Cannot be Null");
            }
            valleyImageParser.loadImage(url, imagePlaceHolder,imageView);
        }
        catch(Exception e)
        {
            Log.e("Error",e.getMessage());
        }

    }

    /**
     * Called when a swipe gesture triggers a refresh.
     */
    @Override
    public void onRefresh() {
        // showing refresh animation before making http call

        swipeRefreshLayout.setRefreshing(true);

        readDoc();
    }


    /**
     * Called when User Clicks the User Instructions Button
     */
    public void addListenerOnButton() {
        Resources userInstructionsMessage = getResources();
        StringBuilder sb=new StringBuilder();
        final String title = "UserInstructions";

        String stringIn = userInstructionsMessage.getString(R.string.user_instructions_message_pinboard);
        final String message = Helper.parseString(stringIn);

        final String okButton = "Ok";
        final String closeButton = "Close";


        userInstructionsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {


                AlertDialog.Builder ad = new AlertDialog.Builder(PinBoardActivity.this);
                ad.setTitle(title);
                ad.setMessage(message);
                ad.setPositiveButton(okButton,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int arg1) {

                            }
                        });
                ad.setNegativeButton(closeButton,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int arg1) {
// do nothing
                            }
                        });
                ad.setCancelable(true);
                ad.setOnCancelListener(new DialogInterface.OnCancelListener() {

                    public void onCancel(DialogInterface dialog) {

                    }
                });

                ad.show();

            }
        });
    }




}
